// Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API

fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((data) => {
	let list = data.map((todo_item) => {
		return todo_item.title
	})
	console.log(list)
})

// Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API

fetch('https://jsonplaceholder.typicode.com/todos/', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'New Title',
		
	})
})
.then((response) => response.json())
.then((data) => console.log(data))

// Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Updated to do list',
	})
})
.then((response) => response.json())
.then((data) => console.log(data))

/*Update a to do list item by changing the data structure to contain 
the following properties:
a. Title
b. Description
c. Status
d. Date Completed
e. User ID*/

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		Title: 'Updated Title',
		Description: 'Updated Description',
		Status: 'Updated Status',
		Date_Completed: 'Oct. 3, 2022',
		User_ID: 1
	})
})
.then((response) => response.json())
.then((data) => console.log(data))




/*Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.*/


fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Corrected to do list',
		completed: true,
		date_changed: 'Oct. 3, 2022'
		
	})

})
.then((response) => response.json())
.then((data) => console.log(data))


/*Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.
*/

fetch('https://jsonplaceholder.typicode.com/todos/1', { 
	method: 'DELETE'
})

